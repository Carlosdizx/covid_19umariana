package java.Mundo;

import java.util.Date;

public interface IPersona
{
    public long darDocumento();

    public void cambiarDocumento( final long pDocumento );

    public String darNombre();

    public void cambiarNombre( final String pNombre );

    public String darDireccion();

    public void cambiarDireccion( final String pDireccion );

    public String darZona();

    public void cambiarZona( final String pZona );

    public String darTelefono();

    public void cambiarTelefono( final String pTelefono );

    public Persona agregarPersona(long pDocumento, String pNombre, String pDireccion, String pZona, String pTelefono)throws Exception;

    public void agregarSintomas( final String pSeniales, final String pDescripcion, final Date pFecha, final boolean pCovid );
}
