package java.DataBase;

import java.sql.*;

public class ConexionBaseDeDatos
{
    //---------------------------------------------Constants---------------------------------------------

    /**
     * Direccion IP del servidor de base de datos
     */
    private final static String DIRECCION = "localhost";

    /**
     * Puerto de conexion a la base de datos
     */
    private final static String PUERTO = "5432";

    /**
     * Nombre de la base de datos
     */
    private final static String NOMBRE = "covid_19Umariana";

    /**
     * URL de conexion para la .
     */
    private final static String URL = "jdbc:postgresql://"+ DIRECCION +":"+ PUERTO +"/"+ NOMBRE;

    /**
     * Usuario de base de datos
     */
    private final static String USER = "postgres";

    /**
     * Contraseña de base de datos
     */
    private final static String PASS = "7423102Ca";

    //---------------------------------------------Attributes---------------------------------------------

    /**
     * Variable de conexion a la base de datos
     */
    private final Connection conexion;

    //---------------------------------------------Constructor---------------------------------------------

    /**
     * Se encarga de inicilizar la variable de conexion y conectar a la base de datos
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws Exception
     */
    public ConexionBaseDeDatos() throws ClassNotFoundException, SQLException, Exception
    {
        Class.forName("org.postgresql.Driver");
        conexion = DriverManager.getConnection(URL, USER, PASS);
        if( conexion == null )
        {
            throw new Exception("No se pudo encontrar la base de datos");
        }
    }

    /**
     * Prepara una sentencia SQL para actualizar la informacion de la base de datos
     * @param pSQL Sentencia SQL
     * @return La sentencia lista para ser consumida
     * @throws SQLException
     */
    public PreparedStatement darSentenciaSQLPreparada( final String pSQL ) throws SQLException
    {
        return conexion.prepareStatement( pSQL );
    }

    /**
     * Hace una consulta en la base de datos dado una sentencia SQL
     * @param pSQL sentencia para ser consultada en la base de datos
     * @return El resultado de la consulta
     * @throws SQLException
     */
    public ResultSet darResultadoDeSentencia( final String pSQL ) throws SQLException
    {
        return conexion.createStatement().executeQuery( pSQL );
    }

    public static void main(String[] args)
    {
        try
        {
            final ConexionBaseDeDatos conexionBaseDeDatos = new ConexionBaseDeDatos();
            System.out.println("Conexion exitosa!");
        }
        catch (Exception e)
        {
            System.err.println( e.getMessage() );
            e.printStackTrace();
        }
    }
}
