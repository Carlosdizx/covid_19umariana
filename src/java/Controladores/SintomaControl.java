package java.Controladores;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

public class SintomaControl extends HttpServlet
{
    private final static UmarinaControlador UMARINA_CONTROLADOR = new UmarinaControlador();


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PersonaControl</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PersonaControl at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public void agregarSintoma( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
    {
        if( UMARINA_CONTROLADOR.esNumero( request.getParameter( "documento" ) ) )
        {
            request.setAttribute( "mensaje", "El documento es incorrecto, solo numeros!" );
            final long documento = Long.parseLong( request.getParameter( "documento" ) );
            final String seniales = request.getParameter( "seniales" );
            final String descripcion = request.getParameter( "descripcion" );
            final boolean covid = request.getParameter( "covid" ) == "si" ? true : false;

            try
            {
                UMARINA_CONTROLADOR.darPersonaDAO().agregarSintoma( documento,seniales,descripcion,covid );
            }
            catch (SQLException throwables)
            {
                request.setAttribute( "mensaje", "Error inexperado, no se pudo hacer el registro: "+throwables.getSQLState() );
            }
        }
        else
        {
            request.setAttribute( "mensaje", "El documento es incorrecto, solo numeros!" );
        }
        request.getRequestDispatcher("registrarSintomas.jsp").forward(request, response);
    }
}
