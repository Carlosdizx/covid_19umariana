package java.Mundo;

import java.util.Date;

public class Persona implements IPersona
{
    private long documento;

    private String nombre;

    private String direccion;

    private String zona;

    private String telefono;

    private Sintoma primero;

    private Sintoma ultimo;

    private Persona izquierda;

    private Persona derecha;

    public Persona( final long pDocumento, final String pNombre, final String pDireccion,
                       final String pZona, final String pTelefono )
    {
        documento = pDocumento;
        nombre = pNombre;
        direccion = pDireccion;
        zona = pZona;
        telefono = pTelefono;
    }

    public long darDocumento()
    {
        return documento;
    }

    public void cambiarDocumento( final long pDocumento )
    {
        documento = pDocumento;
    }

    public String darNombre()
    {
        return nombre;
    }

    public void cambiarNombre( final String pNombre )
    {
        nombre = pNombre;
    }

    public String darDireccion()
    {
        return direccion;
    }

    public void cambiarDireccion( final String pDireccion )
    {
        direccion = pDireccion;
    }

    public String darZona()
    {
        return zona;
    }

    public void cambiarZona( final String pZona )
    {
        zona = pZona;
    }

    public String darTelefono()
    {
        return telefono;
    }

    public void cambiarTelefono( final String pTelefono )
    {
        telefono = pTelefono;
    }

    public Persona agregarPersona(long pDocumento, String pNombre, String pDireccion, String pZona, String pTelefono)throws Exception
    {
        final int comparacion = nombre.compareToIgnoreCase( pNombre );
        if ( documento == pDocumento )
        {
            throw new Exception( "Ya se encuentra registrado la persona '"+pDocumento+"-"+pNombre+"'" );
        }
        if( comparacion == 0 )
        {
            if ( documento > pDocumento )
            {
                return izquierda == null ? izquierda = new Persona(pDocumento,pNombre,pDireccion,pZona,pTelefono)
                        : izquierda.agregarPersona(pDocumento,pNombre,pDireccion,pZona,pTelefono);
            }
            else
            {
                return derecha == null ? derecha = new Persona(pDocumento,pNombre,pDireccion,pZona,pTelefono)
                        : derecha.agregarPersona(pDocumento,pNombre,pDireccion,pZona,pTelefono);
            }
        }
        else if( comparacion > 0 )
        {
            return izquierda == null ? izquierda = new Persona(pDocumento,pNombre,pDireccion,pZona,pTelefono)
                    : izquierda.agregarPersona(pDocumento,pNombre,pDireccion,pZona,pTelefono);
        }
        else
        {
            return derecha == null ? derecha = new Persona(pDocumento,pNombre,pDireccion,pZona,pTelefono)
                    : derecha.agregarPersona(pDocumento,pNombre,pDireccion,pZona,pTelefono);
        }
    }

    public void agregarSintomas(String pSeniales, String pDescripcion, Date pFecha, boolean pCovid)
    {
        if ( ultimo != null )
        {
            ultimo.agregarSintoma( pSeniales, pDescripcion, pFecha, pCovid );
            ultimo = ultimo.darSiguiente();
        }
        else
        {
            primero = new Sintoma( pSeniales, pDescripcion, pFecha, pCovid );
            ultimo = primero;
        }
    }

    public Persona buscarPersona( final long pDocumento, final String pNombre )
    {
        final int comparacion = nombre.compareToIgnoreCase( pNombre );
        if ( comparacion == 0 )
        {
            return this;
        }
        else if ( documento == pDocumento )
        {
            return this;
        }
        else if ( comparacion > 0 )
        {
            return izquierda == null ? null : izquierda.buscarPersona( pDocumento, pNombre );
        }
        else
        {
            return derecha == null ? null : derecha.buscarPersona( pDocumento, pNombre );
        }
    }

    public int contarSintomasRegistrados()
    {
        return primero == null ? 0 : primero.contarSintomasRegistrados( 1 );
    }

    public void recorrido()
    {
        for ( Sintoma s = primero ; s != null ; s = s.darSiguiente() )
        {
            System.out.println( s );
        }
    }

    @Override
    public String toString()
    {
        return "Persona{" +
                "documento=" + documento +
                ", nombre='" + nombre + '\'' +
                '}';
    }

    public void inOrder()
    {
        if ( izquierda != null )
        {
            izquierda.inOrder();
        }
        System.out.println( this );
        if ( derecha != null )
        {
            derecha.inOrder();
        }
    }
}
