package MundoTest;

import java.Mundo.Persona;
import junit.framework.TestCase;

public class PersonaTest extends TestCase
{
    private Persona persona;

    private void crearEscenario1()
    {
        persona = new Persona(1082749257,"Carlos Erenesto Díaz Basante","Consaca, Mz G Casa 5, Los Heroes","Fuera de Pasto", "3163930876");
    }

    private void crearEscenario2()
    {
        persona = new Persona(1082,"Carlos","","Fuera de Pasto", "");
        try
        {
            persona.agregarPersona(1083,"Camilo","","Oeste", "");
            persona.agregarPersona(1084,"Andres","","Oeste", "");
            persona.agregarPersona(1085,"David","","Fuera de Pasto", "");
            persona.agregarPersona(1086,"Faver","","Norte", "");
            persona.agregarPersona(1087,"Oscar","","Sur", "");
            persona.agregarPersona(1088,"Luis Carlos","","Sur", "");
        }
        catch (Exception e)
        {
            fail( "Esto no debio haber ocurrido" );
        }
    }

    public void testAgregarPersona()
    {
        crearEscenario1();
        persona.inOrder();
        System.out.println( "-------------------------------" );
        try
        {
            persona.agregarPersona( 108274925,"Carlos Erenesto Díaz Basante","","Fuera de Pasto", "" );
        }
        catch ( Exception e )
        {
            fail( "El estudiante tenia el mismo nombre pero su documento es distinto, revisar bien el metodo" );
        }
        crearEscenario2();
        persona.inOrder();
        System.out.println( "-------------------------------" );
        try
        {
            persona.agregarPersona( 1084,"Andres Botina","","Centro", "" );
            fail( "El estudiante tenia el mismo nombre pero su documento es distinto, revisar bien el metodo" );
        }
        catch ( Exception e )
        {
           assertTrue( "El estudiante ya se encontraba registrado, con diferente nombre",true );
        }
        try
        {
            persona.agregarPersona( 1090,"David Santacruz","","Sur", "" );
            assertTrue( "El estudiante ya se encontraba registrado, con diferente nombre",true );
            persona.inOrder();
            System.out.println( "-------------------------------" );
        }
        catch ( Exception e )
        {
            fail( "El estudiante tenia el mismo nombre pero su documento es distinto, revisar bien el metodo" );
        }
    }

    public void testBuscarPersona()
    {
        crearEscenario2();
        Persona buscado = persona.buscarPersona( 1084,"" );
        assertEquals( "El estudiante buscado no coincide con el esperado", buscado.darNombre(), "Andres" );
        buscado = persona.buscarPersona( 0,"" );
        assertNull( "El estudiante buscado deberia ser nulo", buscado );
        buscado = persona.buscarPersona( 0,"Andres" );
        assertEquals( "El estudiante buscado no coincide con el esperado", buscado.darDocumento(), 1084 );
        buscado = persona.buscarPersona( 0,"Camilo" );
        assertEquals( "El estudiante buscado no coincide con el esperado", buscado.darDocumento(), 1083 );
        buscado = persona.buscarPersona( 1083,"" );
        assertEquals( "El estudiante buscado no coincide con el esperado", buscado.darNombre(), "Camilo" );

    }

    public void testAgregarSintoma()
    {
        crearEscenario1();
        persona.agregarSintomas( "xd","",null,true );
    }
}
