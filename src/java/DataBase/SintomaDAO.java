package java.DataBase;;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SintomaDAO
{
    private final ConexionBaseDeDatos conexionBaseDeDatos;

    public SintomaDAO(final ConexionBaseDeDatos pConexionBaseDeDatos )
    {
        conexionBaseDeDatos = pConexionBaseDeDatos;
    }

    public void agregarSintoma( final long pDocumento, final String pSeniales, final String pDescripcion, final boolean pCovid ) throws SQLException
    {
        final PreparedStatement statement = conexionBaseDeDatos.darSentenciaSQLPreparada("INSERT INTO sintoma (documento, seniales, descripcion, covid) VALUES (?,?,?,?);");
        statement.setLong( 1, pDocumento );
        statement.setString( 2,pSeniales );
        statement.setString( 3,pDescripcion );
        statement.setBoolean( 4, pCovid );
        statement.executeUpdate();
        statement.close();
    }
}
