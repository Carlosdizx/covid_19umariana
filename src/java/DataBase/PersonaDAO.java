package java.DataBase;

import java.Mundo.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PersonaDAO
{
    private final ConexionBaseDeDatos conexionBaseDeDatos;

    private final SintomaDAO sintomaDAO;

    private Estudiante estudiante;

    private Docente docente;

    private Directivo directivo;

    private Administrativo administrativo;

    public PersonaDAO(final ConexionBaseDeDatos pConexionBaseDeDatos ) throws Exception
    {
        conexionBaseDeDatos = pConexionBaseDeDatos;
        sintomaDAO = new SintomaDAO( conexionBaseDeDatos );
        cargarDatos();
    }

    private void cargarDatos() throws SQLException,Exception
    {
        final ResultSet resultSet = conexionBaseDeDatos.darResultadoDeSentencia( "SELECT documento,persona.nombre,c.nombre,z.nombre,direccion,telefono FROM persona INNER JOIN cargo c on c.id = id_cargo JOIN zona z on z.id = id_zona;" );
        while ( resultSet.next() )
        {
            final long documento = resultSet.getLong( 1 );
            final String nombre = resultSet.getString( 2 );
            final String cargo = resultSet.getString( 3 );
            final String zona = resultSet.getString( 4 );
            final String direccion = resultSet.getString( 5 );
            final String telefono = resultSet.getString( 6 );
            if ( cargo.equals( "Estudiante" ) )
            {
                if (estudiante != null)
                    estudiante.agregarPersona(documento, nombre, zona, direccion, telefono);
                else
                    estudiante = new Estudiante(documento, nombre, zona, direccion, telefono);
            }
            else if ( cargo.equals( "Docente" ) )
            {
                if (docente != null)
                    docente.agregarPersona(documento, nombre, zona, direccion, telefono);
                else
                    docente = new Docente(documento, nombre, zona, direccion, telefono);
            }
            else if ( cargo.equals( "Administrativo" ) )
            {
                if (administrativo != null)
                    administrativo.agregarPersona(documento, nombre, zona, direccion, telefono);
                else
                    administrativo = new Administrativo(documento, nombre, zona, direccion, telefono);
            }
            else
            {
                if (directivo != null)
                    directivo.agregarPersona(documento, nombre, zona, direccion, telefono);
                else
                    directivo = new Directivo(documento, nombre, zona, direccion, telefono);
            }
        }
        resultSet.close();
    }

    public void agregarPersona( final long pDocumento, final String pNombre, final int pIdCargo, final String pDireccion, final int pIdZona, final String pTelefono ) throws Exception
    {
        final PreparedStatement statement = conexionBaseDeDatos.darSentenciaSQLPreparada("INSERT VALUES persona VALUES (?,?,?,?,?,?)");
        statement.setLong( 1, pDocumento );
        statement.setString( 2,pNombre );
        statement.setInt( 3,pIdCargo );
        statement.setString( 4,pDireccion );
        statement.setInt( 5,pIdZona );
        statement.setString( 6,pTelefono );
        statement.executeUpdate();
        statement.close();
        final ResultSet resultSet = conexionBaseDeDatos.darResultadoDeSentencia( "SELECT nombre FROM zona WHERE id="+pIdZona+";" );
        String zona = null;
        while ( resultSet.next() )
            zona = resultSet.getString( resultSet.getString( 1 ) );
        resultSet.close();
        if ( pIdCargo==1 )
        {
            if (estudiante != null)
                estudiante.agregarPersona(pDocumento, pNombre, pDireccion, zona, pTelefono);
            else
                estudiante = new Estudiante( pDocumento, pNombre, pDireccion, zona, pTelefono );
        }
        else if ( pIdCargo==2 )
        {
            if (docente != null)
                docente.agregarPersona(pDocumento, pNombre, pDireccion, zona, pTelefono);
            else
                docente = new Docente( pDocumento, pNombre, pDireccion, zona, pTelefono );
        }
        else if ( pIdCargo==3 )
        {
            if (administrativo != null)
                administrativo.agregarPersona(pDocumento, pNombre, pDireccion, zona, pTelefono);
            else
                administrativo = new Administrativo( pDocumento, pNombre, pDireccion, zona, pTelefono );
        }
        else
        {
            if (directivo != null)
                directivo.agregarPersona(pDocumento, pNombre, pDireccion, zona, pTelefono);
            else
                directivo = new Directivo( pDocumento, pNombre, pDireccion, zona, pTelefono );
        }
    }

    public Persona buscarPersona(final long pDocumento, final String pNombre, final String pCargo )
    {
        if ( pCargo.equals( "Estudiante" ) )
            return estudiante == null ? null : estudiante.buscarPersona( pDocumento, pNombre );
        else if ( pCargo.equals( "Docente" ) )
            return docente == null ? null : docente.buscarPersona( pDocumento, pNombre );
        else if ( pCargo.equals( "Administrativo" ) )
            return administrativo == null ? null : administrativo.buscarPersona( pDocumento, pNombre );
        else
            return directivo == null ? null : directivo.buscarPersona( pDocumento, pNombre );
    }

    public void agregarSintoma( final long pDocumento, final String pSeniales, final String pDescripcion, final boolean pCovid ) throws SQLException
    {
        sintomaDAO.agregarSintoma( pDocumento,pSeniales, pDescripcion, pCovid  );
    }
}