package MundoTest;

import java.Mundo.Sintoma;
import junit.framework.TestCase;

public class SintomaTest extends TestCase
{
    private Sintoma primero;

    private Sintoma ultimo;

    private void crearEscenario1()
    {
        primero = new Sintoma( "Fiebre,Dolor de garganta,","toy enfermo",null,true );
        ultimo = primero;
        ultimo = ultimo.agregarSintoma( "Cansancio,Dolor de pecho","toy enfermo",null,true );
        ultimo = ultimo.agregarSintoma( "Dolor de pecho,Tos,Sudor excesivo","toy enfermo",null,true );
    }

    private void crearEscenario2()
    {
        primero = new Sintoma( "Ninguna","",null,false );
        ultimo = primero;
        ultimo = ultimo.agregarSintoma( "Ninguna","",null,false );
        ultimo = ultimo.agregarSintoma( "Ninguna","",null,false );
    }

    private void crearEscenario3()
    {
        crearEscenario1();
        ultimo = ultimo.agregarSintoma( "Ninguna","",null,false );
        ultimo = ultimo.agregarSintoma( "Ninguna","",null,false );
        ultimo = ultimo.agregarSintoma( "Ninguna","",null,false );
    }

    public void testContarSintomasRegistrados()
    {
        crearEscenario1();
        int casos = primero.contarSintomasRegistrados( 1 );
        assertEquals("Se esperba 3 casos de covid19 registrados",3,casos);
        crearEscenario2();
        casos = primero.contarSintomasRegistrados( 1 );
        assertEquals("Se esperba 0 casos de covid19 registrados",3,casos);
        crearEscenario3();
        ultimo = ultimo.agregarSintoma( "Cansancio,Dolor de pecho","toy enfermo",null,true );
        casos = primero.contarSintomasRegistrados( 1 );
        assertEquals("Se esperba 0 casos de covid19 registrados",7,casos);
    }
}
