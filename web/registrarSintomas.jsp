<%@ page import="java.time.LocalDate" %><%--
    Document   : registrarSintomas
    Created on : 31/10/2020, 01:02:57 PM
    Author     : Carlos Díaz
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registrar Sintomas</title>
        <!-- Bootstrap CSS -->
        <link
                rel="stylesheet"
                href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
                integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"
                crossorigin="anonymous"
        />
        <link rel="stylesheet" href="css/main.css" />
        <link
                rel="stylesheet"
                href="node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css"
        />
    </head>
    <body class="bodyMain">
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
            <button
                    class="navbar-toggler"
                    type="button"
                    data-toggle="collapse"
                    data-target="#navbarSupportedContent"
                    aria-expanded="false"
                    aria-label="Toogle navigation"
            >
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="index.html"><h3>Home</h3></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/covid_19umariana/registrarSintomas.jsp"><h4>Registrar sintomas</h4></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="precios.html"><h4>Consultar sintomas</h4></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><h4>Acerca del Covid19</h4></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contacto.html"><h4>Generar Reporte</h4></a>
                    </li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <form action="registrarSintomas.jsp?accion=agregar" METHOD="post">
                <div class="form-row">
                    <div class="form-group col-sm-6">
                        <label for="documento">Documento</label>
                        <input
                                type="text"
                                name="documento"
                                class="form-control"
                                id="documento"
                                value="<%=request.getParameter("documento") == null ? "" : request.getParameter("documento")%>"
                        />
                    </div>
                    <div class="form-group col-sm-6">
                        <h5><%="La fecha de registro: "+LocalDate.now() %></h5>
                    </div>
                    <div class="form-group col-sm-10">
                        <label>De los siguientes sintomas cuales presenta: </label>
                        <br>
                        <label for="Tos">Tos seca o congestion</label>
                        <input
                                type="checkbox"
                                id="Tos"
                                value="Tos"
                        />
                        <label for="Dolor de cabeza">Dolor de cabeza</label>
                        <input
                                type="checkbox"
                                id="Dolor de cabeza"
                                value="Dolor de cabeza"
                        />
                        <label for="Dolor del cuerpo">Dolor del cuerpo</label>
                        <input
                                type="checkbox"
                                id="Dolor del cuerpo"
                                value="Dolor del cuerpo"
                        />
                        <label for="Fiebre">Fiebre</label>
                        <input
                                type="checkbox"
                                id="Fiebre"
                                value="Fiebre"
                        />
                        <label for="Cansancio">Cansancio</label>
                        <input
                                type="checkbox"
                                id="Cansancio"
                                value="Cansancio"
                        />
                    </div>
                    <div class="form-group col-sm-4">
                        <label>¿Tienes covid🥵😈😡😏😍😂😒😜😘✌👀?</label>
                        <br>
                        <label>Si</label>
                        <input
                                type="checkbox"
                                id="Si"
                                value="Tos"
                        />
                        <label>No</label>
                        <input
                                type="checkbox"
                                id="No"
                                value="Tos"
                        />
                    </div>
                    <div class="form-group col-sm-6">
                        <label>Descripcion de su situacion</label>
                        <textarea
                                id="descripcion"
                                class="form-control"
                                rows="4"
                                maxlength="1000"
                        >
                        </textarea>
                    </div>
                    <div class="form-group col-sm-4"></div>
                    <div class="form-group col-sm-6">
                        <input class="btn btn-success btn-block" type="submit" value="Registrar" />
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
