package java.Controladores;

import java.DataBase.ConexionBaseDeDatos;
import java.DataBase.PersonaDAO;
import java.DataBase.SintomaDAO;

public class UmarinaControlador
{
    private ConexionBaseDeDatos conexionBaseDeDatos;

    private PersonaDAO personaDAO;

    public UmarinaControlador()
    {
        try
        {
            conexionBaseDeDatos = new ConexionBaseDeDatos();
            personaDAO = new PersonaDAO( conexionBaseDeDatos );
        }
        catch (Exception e )
        {
            System.err.println( e.getMessage() );
        }
    }

    protected PersonaDAO darPersonaDAO()
    {
        return personaDAO;
    }

    public boolean esNumero( final String pNumero )
    {
        try
        {
            Long.parseLong( pNumero );
            return true;
        }
        catch ( NumberFormatException e )
        {
            return false;
        }
    }
}
