package java.Mundo;

import java.util.Date;

public class Sintoma
{
    private String seniales;

    private String descripcion;

    private Date fecha;

    private boolean covid;

    private Sintoma siguiente;

    private Sintoma anterior;

    public Sintoma( final String pSeniales, final String pDescripcion, final Date pFecha, final boolean pCovid )
    {
        seniales = pSeniales;
        descripcion = pDescripcion;
        fecha = pFecha;
        covid = pCovid;
    }

    public String darSeniales()
    {
        return seniales;
    }

    public void cambiarSeniales( final String pSeniales )
    {
        seniales = seniales;
    }

    public String darDescripcion()
    {
        return descripcion;
    }

    public void cambiarDescripcion( final String pDescripcion )
    {
        descripcion = descripcion;
    }

    public Date darFecha()
    {
        return fecha;
    }

    public void cambiarFecha( final Date pFecha )
    {
        fecha = fecha;
    }

    public boolean tieneCovid()
    {
        return covid;
    }

    public void cambiarCovid( final boolean pCovid )
    {
        covid = covid;
    }

    public Sintoma darAnterior()
    {
        return anterior;
    }

    public void cambiarAnterior( final Sintoma pAnterior )
    {
        anterior = pAnterior;
    }

    public Sintoma darSiguiente()
    {
        return siguiente;
    }

    public void cambiarSiguiente( final Sintoma pSiguiente )
    {
        siguiente = pSiguiente;
    }

    public Sintoma agregarSintoma( final String pSeniales, final String pDescripcion, final Date pFecha, final boolean pCovid )
    {
        cambiarSiguiente( new Sintoma( pSeniales, pDescripcion, pFecha, pCovid ) );
        siguiente.cambiarAnterior( this );
        return siguiente;
    }

    public String toString()
    {
        return "Sintoma{" +
                "seniales='" + seniales + '\'' +
                ", fecha=" + fecha +
                ", covid=" + covid +
                '}';
    }

    public int contarSintomasRegistrados( int pCantidad )
    {
        return siguiente != null ? siguiente.contarSintomasRegistrados( pCantidad+1 ) : pCantidad;
    }
}
