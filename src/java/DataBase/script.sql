CREATE TABLE cargo
(
    id SERIAL NOT NULL PRIMARY KEY,
    nombre VARCHAR NOT NULL
);

INSERT INTO cargo (nombre) VALUES ('Estudiante');
INSERT INTO cargo (nombre) VALUES ('Docente');
INSERT INTO cargo (nombre) VALUES ('Administrativo');
INSERT INTO cargo (nombre) VALUES ('Direcctivo');

CREATE TABLE zona
(
    id SERIAL NOT NULL PRIMARY KEY,
    nombre VARCHAR(15)
);

INSERT INTO zona (nombre) VALUES ('Sur');
INSERT INTO zona (nombre) VALUES ('Norte');
INSERT INTO zona (nombre) VALUES ('Este');
INSERT INTO zona (nombre) VALUES ('Oeste');
INSERT INTO zona (nombre) VALUES ('Centro');
INSERT INTO zona (nombre) VALUES ('Fuera de Pasto');


CREATE TABLE persona
(
    documento BIGINT NOT NULL PRIMARY KEY,
    nombre VARCHAR(50) NOT NULL,
    id_cargo INT NOT NULL,
    id_zona INT NOT NULL,
    direccion VARCHAR(80) NOT NULL,
    telefono VARCHAR(15) NOT NULL
);

CREATE TABLE sintoma
(
    documento BIGINT NOT NULL,
    seniales VARCHAR(50) NOT NULL DEFAULT 'Ninguna',
    descripcion VARCHAR(100) NOT NULL DEFAULT '',
    fecha TIMESTAMP DEFAULT now(),
    covid BOOLEAN NOT NULL
);